{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE TypeOperators    #-}


import           Data.Extensible      (type (>:))
import           Data.Functor.Indexed ((*>>), ireturn)
import           GHC.OverloadedLabels (fromLabel)

import           Prelude              hiding ((>>), (>>=), fromInteger)
import qualified Prelude

import           Lib

main :: IO ()
main = do
  let prg =
            Let #var (Hask 0)
        *>> Let #var2 (Hask (1 :: Int))
        *>> While (Ref #var :< Hask 10)
              (   (#var := Succ (Ref #var))
              *>> If (Ref #var :< Hask (5 :: Int))
                    (#var2 := Succ (Ref #var2))
                    (ireturn ())
              )

  putStrLn "Result by run:"
  print . snd $ run prg

  putStrLn "\nResult by collectStats:"
  print $ collectStats prg

  putStrLn "\n---\nEquivalent program written in Rebound-do syntax:"

  putStrLn "Result by run:"
  print . snd $ run prgDo

  putStrLn "\nResult by collectStats:"
  print $ collectStats prgDo

 where
  (>>) = (Prelude.>>)
  fromInteger = Prelude.fromInteger


prgDo :: Exp '[] '["var2" >: Int, "var" >: Int] ()
prgDo = do
  Let #var 0
  Let #var2 1
  While (Ref #var :< 10) $ do
    #var := Succ (Ref #var)
    If (Ref #var :< 5)
      (#var2 := Succ (Ref #var2))
      (ireturn ())
 where
  (>>) = (*>>)
  fromInteger = Hask . Prelude.fromInteger
