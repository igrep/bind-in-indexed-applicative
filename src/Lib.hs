{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE ExplicitNamespaces #-}
{-# LANGUAGE GADTs              #-}
{-# LANGUAGE PolyKinds          #-}
{-# LANGUAGE RankNTypes         #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators      #-}

module Lib where

import           Control.Monad.Indexed       ((>>>=))
import           Control.Monad.Indexed.State (IxState, igets, imodify,
                                              runIxState)
import           Data.Bool                   (bool)
import           Data.Coerce
import           Data.Extensible             (type (>:), Assoc, FieldName,
                                              Lookup, Record, nil, itemAssoc, (<:),
                                              (@==))
import           Data.Functor.Const          (Const (Const), getConst)
import           Data.Functor.Indexed        (IxApplicative, IxFunctor,
                                              IxPointed, iap, imap, ireturn,
                                              (*>>), (<<*>>))
import           Data.Functor.Identity (Identity (Identity), runIdentity)
import           Data.Kind                   (Type)
import qualified Data.Map.Strict as M
import           Data.Proxy                  (Proxy (Proxy))
import           GHC.TypeLits                (KnownSymbol, Symbol, symbolVal)


data Exp (xs :: [Assoc Symbol Type]) (ys :: [Assoc Symbol Type]) a where
  Hask :: a -> Exp xs xs a
  Succ :: Exp xs xs Int -> Exp xs xs Int
  (:<) :: Exp xs xs Int -> Exp xs xs Int -> Exp xs xs Bool

  Then :: Exp xs ys a -> Exp ys zs b -> Exp xs zs b
  Fmap :: (a -> b) -> Exp xs ys a -> Exp xs ys b
  Ap   :: Exp xs ys (a -> b) -> Exp ys zs a -> Exp xs zs b

  Let  :: KnownSymbol k => FieldName k -> Exp xs xs a -> Exp xs (k >: a ': xs) ()
  Ref  :: (Lookup xs k v, KnownSymbol k) => FieldName k -> Exp xs xs v
  (:=) :: (Lookup xs k v, KnownSymbol k) => FieldName k -> Exp xs xs v -> Exp xs xs ()

  If    :: Exp xs xs Bool -> Exp xs ys a -> Exp xs ys a -> Exp xs ys a
  While :: Exp xs xs Bool -> Exp xs xs () -> Exp xs xs ()

instance IxFunctor Exp where
  imap = Fmap

instance IxPointed Exp where
  ireturn = Hask

instance IxApplicative Exp where
  iap = Ap


run :: Exp '[] ys a -> (a, Record ys)
run = (`runIxState` nil) . toIxState
 where
  toIxState :: Exp xs ys a -> IxState (Record xs) (Record ys) a
  toIxState (Hask x) = return x
  toIxState (Succ x) = (+ 1) <$> toIxState x
  toIxState (x :< y ) = (<) <$> toIxState x <*> toIxState y

  toIxState (Then fx fy) = toIxState fx *>> toIxState fy
  toIxState (Fmap f fx) = f <$> toIxState fx
  toIxState (Ap ff fx) = toIxState ff <<*>> toIxState fx

  toIxState (Let k mx) = toIxState mx >>>= \x -> imodify (k @== x <:)
  toIxState (Ref k) = igets (^. itemAssoc (fieldNameToProxy k))
  toIxState (k := mx) = toIxState mx >>>= imodify . set (itemAssoc (fieldNameToProxy k))

  toIxState (If cond t f) =
    toIxState cond >>>= bool (toIxState f) (toIxState t)

  toIxState (While cond body) = loop
   where
    cond' = toIxState cond
    body' = toIxState body
    loop  = cond' >>>= bool (ireturn ()) (body' *>> loop)


collectStats :: Exp xs ys a -> M.Map String Int
collectStats = go
 where
  go :: Exp xs ys a -> M.Map String Int
  go (Hask _) = M.empty
  go (Succ x) = go x
  go (x :< y) = go x `merge` go y

  go (Then fx fy) = go fx `merge` go fy
  go (Fmap _ fx) = go fx
  go (Ap ff fx) = go ff `merge` go fx

  go (Let _ mx) = go mx
  go (Ref k) = one k
  go (k := mx) = one k `merge` go mx

  go (If cond t f) = go cond `merge` go t `merge` go f
  go (While cond body) = go cond `merge` go body

  merge = M.unionWith (+)
  one k = M.singleton (symbolVal $ fieldNameToProxy k) 1


indentLines :: String -> String
indentLines = unlines . map ("  " ++) . lines


fieldNameToProxy :: FieldName k -> Proxy k
fieldNameToProxy _ = Proxy


-- | Copied from the lens package
type Getting r s a = (a -> Const r a) -> s -> Const r s

-- | Copied from the lens package
(^.) :: s -> Getting a s a -> a
s ^. l = getConst (l Const s)
{-# INLINE (^.) #-}

-- | Copied from the lens package
type ASetter s t a b = (a -> Identity b) -> s -> Identity t

set :: ASetter s t a b -> b -> s -> t
set l b = runIdentity #. l (\_ -> Identity b)
{-# INLINE set #-}

(#.) :: Coercible c b => (b -> c) -> (a -> b) -> (a -> c)
(#.) _ = coerce (\x -> x :: b) :: forall a b. Coercible b a => a -> b
